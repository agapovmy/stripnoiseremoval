eqwImg = double(rgb2gray(imread('testNoNoise.jpg')))./255;

noise = double(imread('stripNoise.png'))./255;

noisyImg = eqwImg + 0.5.*noise - mean(mean(0.5.*noise));
imwrite(noisyImg, 'testWithNoise.png');
