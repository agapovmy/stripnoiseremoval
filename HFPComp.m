clc, clear;

%Comparsion between High frequency parts

symmetricHFPadj = double(imread('comp/HighFreqPart/B/smoothImage.png'))./255;
zerosHFPadj = double(imread('comp/HighFreqPart/Img/smoothImage.png'))./255;


figure('Name', 'High frequency part symmetric guide img');
imshow(symmetricHFPadj);
figure('Name', 'High frequency part zeros guide img');
imshow(zerosHFPadj);
figure('Name', 'Comparsion');
imshow(symmetricHFPadj - zerosHFPadj);
