clc, clear;
%eqwImg = double(imread('imgLinear.png'))./255;
%eqwImg = double(rgb2gray(imread('imgFromDoc.png')))./255;
eqwImg = double(imread('no_corr.png'))./255;
%eqwImg = double(rgb2gray(imread('imgApprox2.png')))./255;
%eqwImg = double(imread('imgApprox4.png'))./255;
%eqwImg = double(imread('imgCoefBNoStripNoise.png'))./255;

coefB = double(imread('B30.png'))./255;
%coefB = zeros(480, 640);
%coefB = ones(480, 640);

%Guided filtering step 1: Horizontal edge-preserving filtering

r = 12;
r2 = 1;

eps = 0.00001.*power(2,32);

smoothImg = guided_filter(eqwImg, coefB, eps, r, r2);
smoothImgB = guided_filter(eqwImg, zeros(480, 640), eps, r, r2);
text_noise = eqwImg - smoothImg;
text_noiseB = eqwImg - smoothImgB;

%Guided filtering step 2: Vertical strip noise decomposition

r = 1;
r2 = 120;

eps = 0.1.*power(2,32);

stripNoise = guided_filter(text_noise, smoothImg, eps, r, r2);
stripNoiseB = guided_filter(text_noiseB, smoothImgB, eps, r, r2);
imgNoStripNoise = eqwImg - stripNoise;
imgNoStripNoiseB = coefB - stripNoiseB;

figure('Name', 'Image no strip noise'), imshow(imgNoStripNoise);
figure('Name', 'B coef matrix'), imshow(imgNoStripNoiseB);
figure('Name', 'Strip noise of image'), imshow(imadjust(stripNoise));
figure('Name', 'Strip noise of B coef'), imshow(imadjust(stripNoiseB));
figure('Name', 'Compare between noise'), imshow(imadjust(stripNoiseB) - imadjust(stripNoise))
