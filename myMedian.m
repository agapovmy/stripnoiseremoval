function imgOut = myMedian(img, w, h)

%My Median
[hei, wid] = size(img);

wHei = w;
wWid = h;

imgOut = img;

edgex = ceil(wWid./2);
edgey = ceil(wHei./2);
for x = edgex : wid - edgex
    for y = edgey : hei - edgey
        window = zeros(wHei, wWid);
        for fx = 1 : wWid
            for fy = 1 : wHei
                window(fy, fx) = img(y + fy - edgey, x + fx - edgex);
            end
        end
        imgOut(y, x) = mean(mean(window));
    end
end

end
