%eqwImg = double(imread('imgLinear.png'));
eqwImg = double(imread('testWithNoise.png'));
%eqwImg = double(imread('testWithNoise.png')).*257;
%eqwImg = double(imread('sky.png'));
%eqwImg = double(imread('city.png'));
%eqwImg = double(rgb2gray(imread('imgFromDoc.png')));
%eqwImg = double(imread('no_corr.png'))./255;
%eqwImg = double(rgb2gray(imread('imgApprox2.png')))./255;
%eqwImg = double(imread('imgApprox4.png'))./255;
%eqwImg = double(imread('imgCoefBNoStripNoise.png'))./255;
%guide = double(imread('B30.png'))./255;
guide = zeros(480, 640);
%guide = zeros(296, 392);

%Guided filtering step 1: Horizontal edge-preserving filtering

r = 12;
r2 = 1;

eps = 0.1.*power(2,32);
%eps = 0.00001;

%smoothImg = guided_filter_ilya(eqwImg, eqwImg, eps, r, r2);
%smoothImg = guided_filter(eqwImg, eqwImg, eps, r, r2);
smoothImg = double(imguidedfilter(eqwImg, 'NeighborhoodSize',[r2 r], 'DegreeOfSmoothing', 1000));
%smoothImg = guided_filter(eqwImg, guide, eps, r, r2);
text_noise = eqwImg - smoothImg;

%Guided filtering step 2: Vertical strip noise decomposition

r = 1;
r2 = 120;

eps = 0.0001.*power(2,32);
%eps = 0.1;

%stripNoise = guided_filter_ilya(text_noise, smoothImg, eps, r, r2);
%stripNoise = guided_filter(text_noise, smoothImg, eps, r, r2);
stripNoise = double(imguidedfilter(text_noise, smoothImg, 'NeighborhoodSize', [r2 r], 'DegreeOfSmoothing', 10000));

imgNoStripNoise = eqwImg - stripNoise;
%figure, imshow(imgNoStripNoise, [0 65535]);
figure, imshow(imgNoStripNoise, [0 255]);
%figure, imshow(eqwImg);
%figure, imshow(eqwImg - imgNoStripNoise);

%scoreNoFilter = immse(eqwImg, eqwImg2)
%scoreFilter = niqe(eqwImg - imgNoStripNoise)
