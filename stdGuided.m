
file = fopen('../5/30.txt');
img_data = fscanf(file, '%d');
img_data = vec2mat(img_data, 640);
fclose(file);

file = fopen('../5/30T.txt');
T_data = fscanf(file, '%d');
fclose(file);

file = fopen('../coefK_cor.txt');
K_cor = fscanf(file, '%d');
K_cor = vec2mat(K_cor, 640);
fclose(file);

file = fopen('../coefB_T.txt');
B_T = fscanf(file, '%d');
fclose(file);

file = fopen('../coefB.txt');
B_data = fscanf(file, '%d');
B_data = transpose(B_data(3:end));
B = permute(reshape(B_data, 640, 480, 38), [2 1 3]);

fclose(file);

clearvars B_data file;

image = zeros(480, 640);

for i = 1:480
    for B_T_Num = 1:38
        if T_data(i) <= B_T(B_T_Num)
            break;
        end
    end
    B_T_Num = B_T_Num - 1;
    for j = 1:640
        Koef = (T_data(i) - B_T(B_T_Num))./(B_T(B_T_Num+1) - B_T(B_T_Num));
        delta = B(i,j,B_T_Num+1)-B(i,j,B_T_Num);
        delK = (1-Koef).*delta;
        KK = B(i,j,B_T_Num+1) - delK;
        %img_data(i,j) - KK
        image(i,j) = (K_cor(i,j)).*(img_data(i,j) - KK)./32768;
        %if image(i,j) < 0
        %    image(i,j) = 0;
        %end
    end
end



image = mat2gray(image, [min(min(image)) max(max(image))]);

eqwImg = imadjust(image);%Коррекция динамического диапазона

clearvars j i k ans image img_data K_cor KK Koef T_data test delta delK B_T_Num B_T B;


%median = myMedian(eqwImg, 3, 3);
%}
%Guided filtering step 1: Horizontal edge-preserving filtering

%eqwImg = gpuArray(double(imread('image.png'))./255); % GPU Support

eqwImg = double(imread('image.png'))./255;

r = 3;
eps = 0.25;

G = eqwImg; %ones(480, 640); %Guided image
I = eqwImg; %ones(480, 640);
q = imguidedfilter(I, G);

figure, imshow(q);

J = rangefilt(eqwImg);%entropyfilt(eqwImg);%stdfilt(eqwImg); %Image texture

figure, imshow(J);

q2 = imguidedfilter(J, q);

figure, imshow(q2);
%figure, imshow(covIp);

%figure, imshow(median);
%figure, imshow(corrI);

%figure, imshow(eqwImg.*eqwImg);

%Guided filtering step 2: Vertical strip noise decomposition

%{
G = a;
I = q;
meanI = medfilt2(I, [r r]);
meanP = medfilt2(G, [r r]);
corrI = medfilt2(I.*I, [r r]);
corrIp = medfilt2(I.*G, [r r]);

varI = corrI - meanI.*meanI;
covIp = corrIp - meanI.*meanP;

a = covIp./(varI+eps);
b = meanP - a.*meanI;

meanA = medfilt2(a, [r r]);
meanB = medfilt2(b, [r r]);

q = meanA.*I+meanB;

figure, imshow(q);
figure, imshow(a);
%}






