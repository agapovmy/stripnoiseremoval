%test guided
clc, clear

%Impulse
%img = double(imread('imp.png'))./255;
img = double(imread('stepWithCol.png')).*257;

%smooth = guided_filter(img, img, 0.00001, 7, 1);
smooth = guided_filter(img, img, 0.1.*power(2, 32), 12, 1);
%figure, imshow(smooth);
figure, imshow(smooth, [0 65535]);

%{
%img = double(imread('imp.png'))./255;
img = double(imread('imp.png')).*257;

%smooth = guided_filter(img, img, 0.00001, 7, 1);
smooth = guided_filter(img, img, 0.000001.*power(2, 32), 7, 1);
%figure, imshow(smooth);
figure, imshow(smooth, [0 65535]);
%}