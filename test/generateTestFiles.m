%generate test images
clc, clear;

img = cat(2, zeros(480, 320), ones(480, 1), zeros(480, 319));   %imp
imwrite(img, 'imp.png');

img = cat(2, zeros(480, 320), ones(480, 320));                 %step
imwrite(img, 'step.png');

img = cat(2, zeros(480, 210), ones(480, 1), zeros(480, 209), ones(480, 1), zeros(480, 219)); %2 imp
imwrite(img, '2imp.png');

img = cat(2, zeros(480, 160), ones(480, 160), zeros(480, 160), ones(480, 160)); %2 step
imwrite(img, '2step.png');

img = cat(2, zeros(480, 80), ones(480, 80).*0.5, ones(480, 80), ones(480, 80).*0.5, zeros(480, 80), ones(480, 80).*0.5, ones(480, 80), ones(480, 80).*0.5); %3 level
imwrite(img, '3level.png');

img = cat(2, zeros(480, 300), ones(480, 1).*0.2, zeros(480, 19), ones(480, 320)); %step
imwrite(img, 'stepWithCol.png');
