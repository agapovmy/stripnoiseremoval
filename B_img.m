file = fopen('coefB.txt');
B_data = fscanf(file, '%d');
B_data = transpose(B_data(3:end));
B = permute(reshape(B_data, 640, 480, 38), [2 1 3]);

fclose(file);

maxPix = zeros(38);
minPix = zeros(38);

for i=1:38
    minPix(i) = min(min(B(:,:,i)));
    maxPix(i) = max(max(B(:,:,i)));
    %imwrite(mat2gray(B(:,:,i), [minPix(i) maxPix(i)]), strcat('B_img/B_base', int2str(i), '.png'));
end

clearvars B_data file;
BB = zeros(480, 640, 38);

file = fopen('coefB_new.txt', 'w');
fprintf(file, '1\n38\n');

for i = 1:38
    %BB(:,:,i) = mat2gray(B(:,:,i), [min(min(B(:,:,i))) max(max(B(:,:,i)))]);
    BB(:,:,i) = mat2gray(B(:,:,i), [-32768 32767]);

    r = 7;
    r2 = 1;

    eps = 0.00001.*power(2,32);

    smoothImg = guided_filter(BB(:,:,i), BB(:,:,i), eps, r, r2);
    text_noise = BB(:,:,i) - smoothImg;

    %Guided filtering step 2: Vertical strip noise decomposition

    r = 1;
    r2 = 120;

    eps = 0.1.*power(2,32);

    BB(:,:,i) = BB(:,:,i) - guided_filter(text_noise, smoothImg, eps, r, r2);
    BB(:,:,i) = BB(:,:,i).*(65535) - 32768;
    
    for j = 1:480
        for k = 1:640
            fprintf(file, '%d\n', int32(BB(j, k, i)));
        end
    end
    
end

fclose(file);
