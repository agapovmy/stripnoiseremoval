#include <stdio.h>
#include <stdint.h>
#include <string.h>

int main(int argc, char * argv[]) {
    
    if(argc != 3 && argc != 5) {
        printf("Incorrect args\n");
        return 0;
    }
    
    FILE * file1 = NULL;
    if(!(file1 = fopen(argv[1], "rb"))) {
        printf("Can't open first file\n");
        return 0;
    }
    
    FILE * file2 = NULL;
    if(!(file2 = fopen(argv[2], "rb"))) {
        printf("Can't open second file\n");
        return 0;
    }
    
    FILE * outFile = NULL;
    if(argc == 5) {
        int i;
        for(i = 1; i < argc && strcmp(argv[i], "-o"); i++);
        
        if(i == 3) {
            if(!(outFile = fopen(argv[4], "w"))) {
                printf("Can't open output file\n");
                fclose(file1);
                fclose(file2);
                return 0;
            }
        } else {
            printf("Incorrect args\n");
            fclose(file1);
            fclose(file2);
            return 0;
        }
    }
     
    uint8_t f1buf = 0;
    uint8_t f2buf = 0;
    uint64_t cnt = 0;
    
    while( (fread(&f1buf, sizeof(uint8_t), 1, file1)) &&
          (fread(&f2buf, sizeof(uint8_t), 1, file2)) ) {
        if (f1buf != f2buf) {
            printf("file:%s : 0x%lx\n    0x%x\n", argv[1],
                   ftell(file1), f1buf);
            printf("file:%s : 0x%lx\n    0x%x\n\n", argv[2],
                   ftell(file2), f2buf);
            if(outFile) {
                fprintf(outFile, "file:%s : 0x%lx\n    0x%x\n", argv[1],
                        ftell(file1), f1buf);
                fprintf(outFile, "file:%s : 0x%lx\n    0x%x\n\n", argv[2],
                        ftell(file2), f2buf);
            }
            cnt++;
        }
    }
    
    fclose(file1);
    fclose(file2);
    
    printf("Comparsion ends. %ld different bytes was found\n", cnt);
    
    if(outFile){
        fprintf(outFile, "Comparsion ends. %ld different bytes was found\n",
                cnt);
        fclose(outFile);
    }
    
    return 0;
}
