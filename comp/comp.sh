#!/bin/bash

if ! [ -n "$1" ]
then
	echo "No args. Defaul file is highFreqPartAdjust.png"
	echo $1
	fileName="highFreqPartAdjust.png"
else
	fileName="$1"
fi


if ! [ -d byteComp.d/ ]
then
	mkdir byteComp.d
fi

./byteComp HighFreqPart/B/$fileName HighFreqPart/Img/$fileName -o byteComp.d/B_v_Img.out -nocon
./byteComp HighFreqPart/B/$fileName HighFreqPart/Ones/$fileName -o byteComp.d/B_v_Ones.out -nocon
./byteComp HighFreqPart/B/$fileName HighFreqPart/Zeros/$fileName -o byteComp.d/B_v_Zeros.out -nocon
./byteComp HighFreqPart/Img/$fileName HighFreqPart/Ones/$fileName -o byteComp.d/Img_v_Ones.out -nocon
./byteComp HighFreqPart/Img/$fileName HighFreqPart/Zeros/$fileName -o byteComp.d/Img_v_Zeros.out -nocon
./byteComp HighFreqPart/Ones/$fileName HighFreqPart/Zeros/$fileName -o byteComp.d/Ones_v_Zeros.out -nocon
