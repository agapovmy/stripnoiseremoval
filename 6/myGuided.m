clc;
clear;

file = fopen('../../4/30.txt');
img_data = fscanf(file, '%d');
img_data = vec2mat(img_data, 640);
fclose(file);

file = fopen('../../4/30T.txt');
T_data = fscanf(file, '%d');
fclose(file);

file = fopen('../coefK_cor.txt');
K_cor = fscanf(file, '%d');
K_cor = vec2mat(K_cor, 640);
fclose(file);

file = fopen('../coefB_T.txt');
B_T = fscanf(file, '%d');
fclose(file);

file = fopen('../coefB.txt');
B_data = fscanf(file, '%d');
B_data = transpose(B_data(3:end));
B = permute(reshape(B_data, 640, 480, 38), [2 1 3]);

fclose(file);

clearvars B_data file;

%{ 
%FOR B COEF FILTERING
BB = zeros(480, 640, 38);
for i = 1:38
    %BB(:,:,i) = mat2gray(B(:,:,i), [min(min(B(:,:,i))) max(max(B(:,:,i)))]);
    BB(:,:,i) = B(:,:,i);

    r = 12;
    r2 = 1;

    eps = 0.00001.*power(2,32);

    smoothImg = guided_filter(BB(:,:,i), BB(:,:,i), eps, r, r2);
    text_noise = BB(:,:,i) - smoothImg;

    %Guided filtering step 2: Vertical strip noise decomposition

    r = 1;
    r2 = 120;

    eps = 0.1.*power(2,32);

    BB(:,:,i) = BB(:,:,i) - guided_filter(text_noise, smoothImg, eps, r, r2);
end

B = BB;
clearvars BB;
%}

image = zeros(480, 640);

%dotsx = zeros(1, 38);
%for i = 1:38
%    dotsx(i) = i;
%end

B_curr_img = zeros(480, 640);

for i = 1:480
    for B_T_Num = 1:38
        if T_data(i) <= B_T(B_T_Num)
            break;
        end
    end
    B_T_Num = B_T_Num - 1;
    for j = 1:640
        %dotsy = squeeze(B(i,j,:))';
        
        %FOR APPROX
        %p = polyfit(dotsx, dotsy, 4);
        Koef = (T_data(i) - B_T(B_T_Num))./(B_T(B_T_Num+1) - B_T(B_T_Num));
        %pos = B_T_Num + Koef;
        
        %FOR LINER INTERP
        delta = B(i,j,B_T_Num+1)-B(i,j,B_T_Num);
        delK = (1-Koef).*delta;
        KK = B(i,j,B_T_Num+1) - delK;
        image(i,j) = (K_cor(i,j)).*(img_data(i,j) - KK)./32768;
        %FOR B_CURR_IMG
        B_curr_img(i,j) = KK;
        
        %FOR APPROX
        %image(i,j) = (K_cor(i,j)).*(img_data(i,j) - polyval(p, pos))./32768;
        
        %FOR INTERP1
        %Koef = (T_data(i) - B_T(B_T_Num))./(B_T(B_T_Num+1) - B_T(B_T_Num));
        %pos = B_T_Num + Koef;
        %image(i,j) = (K_cor(i,j)).*(img_data(i,j) - interp1(dotsx, dotsy, pos, 'pchip'))./32768;
    end
end

%min_im = min(min(image));

%for i = 1:480
%    for j = 1:640
%        image(i, j) = image(i,j) - min_im;
%    end
%end

image = mat2gray(image, [min(min(image)) max(max(image))]);


%figure;


%figure
%subplot(2,2,1), imshow(image);
%subplot(2,2,2), imhist(image);   %Гистограмма
eqwImg = imadjust(image);%Коррекция динамического диапазона
%meanVal = mean(mean(eqwImg));

%for i=1:480
%    for j=1:640
%        tempPix = 
%    end
%end
%eqwImg = histeq(image);  %Эквализация
%figure
%subplot(2,2,3), imshow(eqwImg);         %Новое изображения
%subplot(2,2,4), imhist(eqwImg);         %Новая гистограмма
%figure, imshowpair(image(:,:,i), eqwImg, 'diff');

%figure, imshow(eqwImg);
%G = triu(ones(480, 640));
%guidedImg = imguidedfilter(eqwImg, G);
%figure, imshow(guidedImg);
%figure, imshow(imguidedfilter(eqwImg));

%imwrite(eqwImg, 'image.png');

%figure, imshowpair(image(:,:,1), image(:,:,2), 'diff');

clearvars j i k ans image img_data K_cor KK Koef T_data test delta delK B_T_Num B_T %B;

%median = myMedian(eqwImg, 3, 3);

%Guided filtering step 1: Horizontal edge-preserving filtering

r = 2;
eps = 0.01;

G = eqwImg; %ones(480, 640); %Guided image
I = eqwImg; %ones(480, 640);
meanI = myMedian(I, r, r);
meanP = myMedian(G, r, r);
corrI = myMedian(I.*I, r, r);
corrIp = myMedian(I.*G, r, r);

varI = corrI - meanI.*meanI;
covIp = corrIp - meanI.*meanP;

a = covIp./(varI+eps);
b = meanP - a.*meanI;

meanA = myMedian(a, r, r);
meanB = myMedian(b, r, r);

q = meanA.*I+meanB;

%figure, imshow(q);
%figure, imshow(a);

%figure, imshow(varI);
%figure, imshow(covIp);

%figure, imshow(median);
%figure, imshow(corrI);

%figure, imshow(eqwImg.*eqwImg);

%Guided filtering step 2: Vertical strip noise decomposition

G = a; %ones(480, 640); %Guided image
I = q; %ones(480, 640);
meanI = myMedian(I, r, r);
meanP = myMedian(G, r, r);
corrI = myMedian(I.*I, r, r);
corrIp = myMedian(I.*G, r, r);

varI = corrI - meanI.*meanI;
covIp = corrIp - meanI.*meanP;

a = covIp./(varI+eps);
b = meanP - a.*meanI;

meanA = myMedian(a, r, r);
meanB = myMedian(b, r, r);

q = meanA.*I+meanB;

figure, imshow(q);
%figure, imshow(a);






