# StripNoiseRemoval

Description:
Implementation of an algorithm from the article "Effective Strip Noise Removal
for Low-Textured Infrared Images Based on 1-D Guided Filtering" 
IEEE TRANSACTIONS ON CIRCUITS AND SYSTEMS FOR VIDEO TECHNOLOGY, VOL. 26, NO. 12, DECEMBER 2016
