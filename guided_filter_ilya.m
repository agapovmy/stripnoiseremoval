% guided_filter - two dimensional edge-preserving filtering
%    This function implements 2-D guided filtering using
%    the method outlined in:
%      'Guided Image Filtering' by He, K; Sun, J and Tang, X; ECCV 2010
% 
% filtered = guided_filter(input, guide, epsilon, win_size)
% input - the input image to be filtered, assumed to be single channel
%         intensity image, with values in [0 1].
% guide - the guide image, same dimension and same range as 'input'.
% epsilon - the regularization parameter to provide the balance between
%           data matching and smoothing, note epsilon ~= 0, otherwise 
%           division by zero error occurs when calculating average a in 
%           flat image region. 
% win_size - size of the windows, odd numbers only, eg., 3, 5, 7.
%
% Ruimin Pan, Canon Information System Research Australia, September 2011.
% ruimin.pan AT cisra.canon.com.au


function [filtered, mua, b] = guided_filter_ilya(input, guide, epsilon, win_Ysize, win_Xsize)
% calculate a few useful parameters
halfX = floor(win_Xsize / 2);
halfY = floor(win_Ysize / 2);
% average value in every win_size-by-win_size window of the input image
paddedp = padarray(input, [halfY, halfX], 'both');
mup = zeros(size(paddedp));
% average value in every win_size-by-win_size window of the guide image
paddedi = padarray(guide, [halfY, halfX], 'both');
mui = zeros(size(paddedi));
% variance in every window of the guide image
sigmai = zeros(size(paddedi));
% cross term of the guide and input image in every window
cross = zeros(size(paddedi));

%constructing denominator image;
initial_denom = padarray(ones(size(input)), [halfY, halfX], 'both');
denom = zeros(size(paddedi));

% calculating sum over each window by shifting and adding
for i = -halfY : halfY
    for j = -halfX : halfX
        mup = mup + circshift(paddedp, [i, j]);
        mui = mui + circshift(paddedi, [i, j]);
        sigmai = sigmai + circshift(paddedi, [i, j]).^2;
        cross = cross + circshift(paddedi, [i, j]).*circshift(paddedp, [i, j]);
        denom = denom + circshift(initial_denom, [i, j]);
    end
end

% calculate average, variance and cross terms in equation (5) and (6) in
% the paper
mup = mup ./ denom;
mui = mui ./ denom;
sigmai = sigmai ./ denom - mui.^2;
cross = cross ./ denom;

% remove the padding
mup = mup(halfY+1:end-halfY, halfX+1:end-halfX);
mui = mui(halfY+1:end-halfY, halfX+1:end-halfX);
sigmai = sigmai(halfY+1:end-halfY, halfX+1:end-halfX);
cross = cross(halfY+1:end-halfY, halfX+1:end-halfX);
denom = denom(halfY+1:end-halfY, halfX+1:end-halfX);

% calculating the linear coefficients a and b
a = (cross - mui .* mup) ./ (sigmai + epsilon);
b = mup - a .* mui;

apad = padarray(a, [halfY, halfX], 'both');
bpad = padarray(b, [halfY, halfX], 'both');

mua = zeros(size(apad));
mub = zeros(size(bpad));

% calculating sum over each window by shifting and adding
for i = -halfY : halfY
    for j = -halfX : halfX
        mua = mua + circshift(apad, [i, j]);
        mub = mub + circshift(bpad, [i, j]);
    end
end

% remove the padding
mua = mua(halfY+1:end-halfY, halfX+1:end-halfX);
mub = mub(halfY+1:end-halfY, halfX+1:end-halfX);

% calculate average a and b
mua = mua ./ denom;
mub = mub ./ denom;

% the filtered image
filtered = mua .* guide + mub;