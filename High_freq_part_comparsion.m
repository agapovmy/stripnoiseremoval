clc, clear;

eqwImg = double(imread('imgLinear.png'))./255;
%eqwImg = double(rgb2gray(imread('imgFromDoc.png')))./255;
%eqwImg = double(imread('no_corr.png'))./255;
%eqwImg = double(rgb2gray(imread('imgApprox2.png')))./255;
%eqwImg = double(imread('imgApprox4.png'))./255;
%eqwImg = double(imread('imgCoefBNoStripNoise.png'))./255;

%guide = double(imread('B30.png'))./255;
%guide = zeros(480, 640);
guide = ones(480, 640);

%Guided filtering step 1: Horizontal edge-preserving filtering

r = 12;
r2 = 1;

eps = 0.00001.*power(2,32);

smoothImgOnes = guided_filter(eqwImg, guide, eps, r, r2);
highFreqPartOnes = eqwImg - smoothImgOnes;

figure('Name', 'Smooth image ones'), imshow(smoothImgOnes);
figure('Name', 'High frequency part guide ones'), imshow(imadjust(highFreqPartOnes));

guide = zeros(480, 640);

smoothImgZeros = guided_filter(eqwImg, guide, eps, r, r2);
highFreqPartZeros = eqwImg - smoothImgZeros;

figure('Name', 'Smooth image zeros'), imshow(smoothImgZeros);
figure('Name', 'High frequency part guide zeros'), imshow(imadjust(highFreqPartZeros));

%{
figure('Name', 'Comparsion beetween high freq parts');
subplot(1, 2, 1);
imshow(imadjust(smoothImgZeros - smoothImgOnes));
title('Comp img');

subplot(1, 2, 2);
imhist(imadjust(smoothImgZeros - smoothImgOnes));
title('Histogram');
%}

guide = eqwImg;

smoothImgImg = guided_filter(eqwImg, guide, eps, r, r2);
highFreqPartImg = eqwImg - smoothImgImg;

figure('Name', 'Smooth image image'), imshow(smoothImgImg);
figure('Name', 'High frequency part self guidance'), imshow(imadjust(highFreqPartImg));

guide = double(imread('B30.png'))./255;

smoothImgB = guided_filter(eqwImg, guide, eps, r, r2);
highFreqPartB = eqwImg - smoothImgB;

figure('Name', 'Smooth image B'), imshow(smoothImgB);
figure('Name', 'High frequency part guide B'), imshow(imadjust(highFreqPartB));

%Save block
imwrite(smoothImgOnes, 'comp/HighFreqPart/Ones/smoothImage.png');
imwrite(highFreqPartOnes, 'comp/HighFreqPart/Ones/highFreqPart.png');
imwrite(imadjust(highFreqPartOnes), 'comp/HighFreqPart/Ones/highFreqPartAdjust.png');
imwrite(smoothImgZeros, 'comp/HighFreqPart/Zeros/smoothImage.png');
imwrite(highFreqPartZeros, 'comp/HighFreqPart/Zeros/highFreqPart.png'); 
imwrite(imadjust(highFreqPartZeros), 'comp/HighFreqPart/Zeros/highFreqPartAdjust.png');
imwrite(smoothImgImg, 'comp/HighFreqPart/Img/smoothImage.png');
imwrite(highFreqPartImg, 'comp/HighFreqPart/Img/highFreqPart.png'); 
imwrite(imadjust(highFreqPartImg), 'comp/HighFreqPart/Img/highFreqPartAdjust.png');
imwrite(smoothImgB, 'comp/HighFreqPart/B/smoothImage.png');
imwrite(highFreqPartB, 'comp/HighFreqPart/B/highFreqPart.png'); 
imwrite(imadjust(highFreqPartB), 'comp/HighFreqPart/B/highFreqPartAdjust.png');





