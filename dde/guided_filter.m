% guided_filter - two dimensional edge-preserving filtering
%    This function implements 2-D guided filtering using
%    the method outlined in:
%      'Guided Image Filtering' by He, K; Sun, J and Tang, X; ECCV 2010
% 
% filtered = guided_filter(input, guide, epsilon, win_size)
% input - the input image to be filtered, assumed to be single channel
%         intensity image, with values in [0 1].
% guide - the guide image, same dimension and same range as 'input'.
% epsilon - the regularization parameter to provide the balance between
%           data matching and smoothing, note epsilon ~= 0, otherwise 
%           division by zero error occurs when calculating average a in 
%           flat image region. 
% win_size - size of the windows, odd numbers only, eg., 3, 5, 7.
%
% Ruimin Pan, Canon Information System Research Australia, September 2011.
% ruimin.pan AT cisra.canon.com.au


function filtered = guided_filter(input, guide, epsilon, win_wid, win_hei)
% calculate a few useful parameters
half_hei = floor(win_hei / 2);
half_wid = floor(win_wid / 2);
% average value in every win_wid-by-win_hei window of the input image
paddedp = padarray(input, [half_hei, half_wid], 'both');
mup = zeros(size(paddedp));
% average value in every win_wid-by-win_hei window of the guide image
paddedi = padarray(guide, [half_hei, half_wid], 'both');
mui = zeros(size(paddedi));
% variance in every window of the guide image
sigmai = zeros(size(paddedi));
% cross term of the guide and input image in every window
cross = zeros(size(paddedi));

%constructing denominator image;
initial_denom = padarray(ones(size(input)), [half_hei, half_wid], 'both');
denom = zeros(size(paddedi));


% calculating sum over each window by shifting and adding
for i = -half_hei : half_hei
    for j = -half_wid : half_wid
        mup = mup + circshift(paddedp, [i, j]);
        mui = mui + circshift(paddedi, [i, j]);
        sigmai = sigmai + circshift(paddedi, [i, j]).^2;
        cross = cross + circshift(paddedi, [i, j]).*circshift(paddedp, [i, j]);
        denom = denom + circshift(initial_denom, [i, j]);
    end
end

% remove the padding
mup = mup(half_hei+1:end-half_hei, half_wid+1:end-half_wid);
mui = mui(half_hei+1:end-half_hei, half_wid+1:end-half_wid);
sigmai = sigmai(half_hei+1:end-half_hei, half_wid+1:end-half_wid);
cross = cross(half_hei+1:end-half_hei, half_wid+1:end-half_wid);
denom = denom(half_hei+1:end-half_hei, half_wid+1:end-half_wid);

% calculate average, variance and cross terms in equation (5) and (6) in
% the paper
mup = mup ./ denom;
mui = mui ./ denom;
sigmai = sigmai ./ denom - mui.^2;
cross = cross ./ denom;

% calculating the linear coefficients a and b
a = (cross - mui .* mup) ./ (sigmai + epsilon);
b = mup - a .* mui;

apad = padarray(a, [half_hei, half_wid], 'both');
bpad = padarray(b, [half_hei, half_wid], 'both');

mua = zeros(size(apad));
mub = zeros(size(bpad));

% calculating sum over each window by shifting and adding
for i = -half_hei : half_hei
    for j = -half_wid : half_wid
        mua = mua + circshift(apad, [i, j]);
        mub = mub + circshift(bpad, [i, j]);
    end
end

% remove the padding
mua = mua(half_hei+1:end-half_hei, half_wid+1:end-half_wid);
mub = mub(half_hei+1:end-half_hei, half_wid+1:end-half_wid);

% calculate average a and b
mua = mua ./ denom;
mub = mub ./ denom;

% the filtered image
filtered = mua .* guide + mub;
  
    
    











