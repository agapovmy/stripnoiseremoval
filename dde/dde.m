%DDE

%img = double(rgb2gray(imread('ddeArticle.png'))).*257;
%img = double(rgb2gray(imread('img43.jpg'))).*257;
%img = double(rgb2gray(imread('../testNoNoise.jpg'))).*257;
%img = double(imread('../imgLinear.png')).*257;
%img = double(imread('military.png')).*257;
%img = double(imread('tower.png')).*257;
%img = double(imread('runningMan.png')).*257;

%img = readFromText('calibratedImg', 640);
img = readFromText('calibratedImgNonAdjust', 640);

figure, imshow(img, [0 65535]);

[imHei, imWid] = size(img);
%T = imHei.*imWid./1000;
%T = 1;
 T = 3;

%High frequency part
%winsize = 3
%e = 100
e = power(2,32);
winsize = 7;

[~, ~, distrib] = histProjFast(img, T, 0, 65535, 1, 0);
for i = 1:imWid
    for j = 1:imHei
        img(j, i) = 255.*distrib(img(j, i) + 1);
    end
end
img = uint8(img);

smooth = round(guided_filter(double(img), double(img), e, winsize, winsize));
smoothInt = (uint16(smooth));
[Nvalid, hist, distrib] = histProjFast(smoothInt, T, 0, 65535, 1, 0);
hfp = uint8((double(img) - smooth)./257);
%figure, imshow(smooth, [0 65535]);
%figure, imshow(binarized, [0 65535]);
%figure, imhist(smooth./65535);
%hfp = histeq((img - smooth)./65535).*65535;

for i = 1:imWid
    for j = 1:imHei
        smoothInt(j, i) = 255.*distrib(smoothInt(j, i) + 1);
    end
end
smoothInt = uint8(smoothInt);

figure, imshow(smoothInt - 0);
figure, imshow(uint8(smoothInt) + hfp);
% figure, imshow(img + img - smooth, [0 65535]);
