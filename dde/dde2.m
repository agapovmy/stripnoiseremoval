%img = readFromText('calibratedImgNonAdjust', 640);
img = readFromText('highway', 640);

[imHei, imWid] = size(img);
%T = imHei.*imWid./1000;
%T = 1;
 T = 3;

%High frequency part
%winsize = 7
%e = 650
e = power(2,32);
winsize = 7;

[~, ~, distrib] = histProjFast(img, T);
for i = 1:imWid
    for j = 1:imHei
        img(j, i) = 255.*distrib(img(j, i) + 1);
    end
end
img = uint8(img);

img = imread('tower.png');

smooth = ForHDL(img, 1000);

smooth = imguidedfilter(img, 'NeighborhoodSize',[7 7]);

hfp = imguidedfilter((img-smooth).*(115./max(max(img-smooth))), 'NeighborhoodSize',[3 3], 'DegreeOfSmoothing', 200);

out = smooth + hfp;

figure, imshow(img)
figure, imshow(out)
