%binarize

function [Nvalid, hist, distrib] = histProj(input, T, imMin, imMax, step, eps)

[imWid, imHei] = size(input);
hist = logical(zeros(65536, 1));
distrib = zeros(65536, 1);

Nvalid = 0;
for lev = imMin:step:imMax
    Nx = 0;
    for i = 1:imWid
        for j = 1:imHei
            if input(i, j) >= lev-eps && input(i, j) <= lev+eps
                Nx = Nx + 1;
            end
        end
    end
    if Nx >= T
        Nvalid = Nvalid + 1;
        hist(lev+1) = true;
    end
    distrib(lev+1) = Nvalid;
end

distrib = distrib./Nvalid;
