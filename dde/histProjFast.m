%Fast Histogram Projection
function [Nvalid, hist, distrib] = histProjFast(input, T)

[imHei, imWid] = size(input);
Nvalid = 0;
hist = logical(zeros(65536, 1));
distrib = zeros(65536, 1);

levelCnt = zeros(65536, 1);

%Binary histogram
for i = 1:imHei
    for j = 1:imWid
        levelCnt(input(i, j) + 1) = levelCnt(input(i, j) + 1) + 1;
        if not(hist(input(i, j) + 1)) && levelCnt(input(i, j) + 1) >= T
            hist(input(i, j) + 1) = true;
            Nvalid = Nvalid + 1;
        end
    end
end

clearvars levelCnt;
%Distribution
Hy = 0;
for i = 1:65536
    if hist(i)
        Hy = Hy + 1;
    end
    distrib(i) = Hy;
end

distrib = distrib./Nvalid;
