testArray = uint8(horzcat(zeros(1,100), 255*ones(1,100)));
M = 127;

dots = testArray;
%plot(1:200, dots);
%hold on;

eps = 1;
eps_arr = 1;
for i = 1:100
    filtered = imguidedfilter(testArray, 'NeighborhoodSize',[1 8], 'DegreeOfSmoothing', eps);
    dots = cat(1, dots, filtered);
    %plot(1:200, dots(i + 1, :));
    eps = eps .* power(10, 0.1);
    eps_arr = cat(2, eps_arr, eps);
end

surf(dots, 'FaceColor', 'interp');

baseImg = rgb2gray(imread('../testNoNoise.jpg'));
noisyImg = double(imread('../testWithNoise.png'));
eps1 = 10;
eps2 = 10;
ssimDots = 0;
testImg = double(imread('../city.png'));
res = zeros(9, 480, 640);

for i = 1:3
    eps2 = 10;
    for j = 1:3
        smooth = imguidedfilter(testImg, 'NeighborhoodSize',[1 12], 'DegreeOfSmoothing', eps1);
        hiFreq = testImg - smooth;
        filtered = testImg - imguidedfilter(hiFreq, smooth, 'NeighborhoodSize',[120 1], 'DegreeOfSmoothing', eps2);
        res((i-1).*3 + j, :, :) = filtered;
        for k = 1:j
            eps2 = eps2 .* 10;
        end
    end
    for k = 1:i
        eps1 = eps1 .* 10;
    end
end

result = cat(2, squeeze(res(1,121:360,161:480)), squeeze(res(2,121:360,161:480)), squeeze(res(3,121:360,161:480)));
result = cat(1, result, cat(2, squeeze(res(4,121:360,161:480)), squeeze(res(5,121:360,161:480)), squeeze(res(6,121:360,161:480))));
result = cat(1, result, cat(2, squeeze(res(7,121:360,161:480)), squeeze(res(8,121:360,161:480)), squeeze(res(9,121:360,161:480))));
result(:, 320) = 0;
result(:, 640) = 0;
result(240, :) = 0;
result(480, :) = 0;
imshow(result, [0 255])

for i = 1:100
    filtered = uint8(imguidedfilter(int16(noisyImg), 'NeighborhoodSize',[1 8], 'DegreeOfSmoothing', eps1));
    ssimDots = cat(2, ssimDots, ssim(filtered, baseImg));
    eps1 = eps1 .* power(10, 0.1);
    eps_arr = cat(2, eps_arr, eps1);
end

figure, plot(ssimDots(2:end));

eps1 = 1;
eps2 = 1;
ssimArr = 0;
ssimFull = zeros(1,100);
eps1_arr = ones(1, 100);
eps2_arr = 1;

for i = 1:100
    eps2 = 1;
    ssimArr = 0;
    for j = 1:100
        smooth = imguidedfilter(noisyImg, 'NeighborhoodSize',[1 7], 'DegreeOfSmoothing', eps1);
        hiFreq = baseImg - smooth;
        filtered = baseImg - imguidedfilter(hiFreq, smooth, 'NeighborhoodSize',[100 1], 'DegreeOfSmoothing', eps2);
        ssimArr = cat(2, ssimArr, ssim(filtered, baseImg));
        eps2 = eps2 .* power(10, 0.1);
    end
    eps1 = eps1 .* power(10, 0.1);
    ssimFull = cat(1, ssimFull, ssimArr(2:end));
    eps2_arr = cat(2, eps2_arr, eps1);
end

ssimFull = ssimFull(2:end, 1:end).*10000;
surf(ssimFull, 'FaceColor', 'interp');
%ssimArr = ssimArr(2:end);
%figure, plot(ssimArr);
%figure, loglog(ssimArr);
