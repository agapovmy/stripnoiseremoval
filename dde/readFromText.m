%read from text

function image = readFromText(filename, wid)

file = fopen(filename);
image = fscanf(file, '%d');
image = uint16(vec2mat(image, wid));
fclose(file);
